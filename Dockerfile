# Use the official Rust image as the base image
FROM rust:latest

# Set the working directory
WORKDIR /app

# Copy the application source code to the working directory
COPY . .

# Build the Rust application
RUN cargo build --release

# Set the command to run when the container starts
CMD ["./target/release/prime-number-api"]

